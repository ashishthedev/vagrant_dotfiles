" vim-plug configuration
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

Plug 'pearofducks/ansible-vim'
Plug 'tpope/vim-fugitive'
Plug 'scrooloose/nerdtree'
Plug 'vim-syntastic/syntastic'

" Add plugins to &runtimepath
call plug#end()

" Leader
let mapleader = "\\"

set backspace=2   " Backspace deletes like most programs in insert mode
set nobackup
set nowritebackup
set noswapfile    " http://robots.thoughtbot.com/post/18739402579/global-gitignore#comment-458413287
set history=50
set ruler         " show the cursor position all the time
set showcmd       " display incomplete commands
set incsearch     " do incremental searching
set laststatus=2  " Always display the status line
set autowrite     " Automatically :write before running commands
set pastetoggle=<F10>
set mouse=a       " Enable mouse in all modes
set ttymouse=xterm2

" Color adjustments
syntax enable
set background=dark
colorscheme solarized

if filereadable(expand("~/.vimrc.bundles"))
  source ~/.vimrc.bundles
endif

filetype plugin indent on

augroup vimrcEx
  autocmd!

 " When editing a file, always jump to the last known cursor position.
 " Don't do it for commit messages, when the position is invalid, or when
 " inside an event handler (happens when dropping a file on gvim).
  autocmd BufReadPost *
    \ if &ft != 'gitcommit' && line("'\"") > 0 && line("'\"") <= line("$") |
    \   exe "normal g`\"" |
    \ endif

 " Set syntax highlighting for specific file types
  autocmd BufRead,BufNewFile Appraisals set filetype=ruby
  autocmd BufRead,BufNewFile *.md set filetype=markdown
  autocmd BufRead,BufNewFile .{jscs,jshint,eslint}rc set filetype=json
  autocmd BufRead,BufNewFile .j2 set filetype=jinja2
augroup END

" When the type of shell script is /bin/sh, assume a POSIX-compatible
" shell for syntax highlighting purposes.
let g:is_posix = 1

" Expand tab to 2 spaces
set tabstop=2
set softtabstop=2
set expandtab
set shiftwidth=2
set smarttab

" Display extra whitespace and tabs as characters, but not by default
set list
set listchars=tab:»·,trail:·,nbsp:·

" Use one space, not two, after punctuation.
set nojoinspaces

" Numbers
set number
set numberwidth=5

" Get off my lawn
nnoremap <Left> :echoe "Use h"<CR>
nnoremap <Right> :echoe "Use l"<CR>
nnoremap <Up> :echoe "Use k"<CR>
nnoremap <Down> :echoe "Use j"<CR>

" Run commands that require an interactive shell
nnoremap <Leader>r :RunInInteractiveShell<space>

" Treat <li> and <p> tags like the block tags they are
let g:html_indent_tags = 'li\|p'

" Open new split panes to right and bottom, which feels more natural
set splitbelow
set splitright

nnoremap <C-n> <c-w><
nnoremap <C-m> <c-w>>

" Quicker window movement
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l

" configure syntastic syntax checking to check on open as well as save
let g:syntastic_check_on_open=1
let g:syntastic_html_tidy_ignore_errors=[" proprietary attribute \"ng-"]
let g:syntastic_eruby_ruby_quiet_messages =
    \ {"regex": "possibly useless use of a variable in void context"}

" Always use vertical diffs
set diffopt+=vertical

" Set the statusline
set statusline=%f         " Path to the file
set statusline+=%y        " Filetype of the file
set statusline+=%=        " Switch to the right side
set statusline+=Current:\ %-4l " Display current line
set statusline+=Total:\ %-4L " Dispay total lines
set statusline+=%{fugitive#statusline()} " Git status

" map nerdtree viewport to CTRL+n

map <C-t> :NERDTreeToggle<CR>

" Options for syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0



set nu

"Maps
map \\ :q!<CR>
map <leader>v :tabedit ~/.vimrc<CR>
map <space> /
map ,re :source ~/.vimrc<CR>
imap <tab> <C-P>
map j gj
map k gk

noremap <F2> :%s/\<<C-R><C-W>\>/<C-R><C-w>/gc
noremap <F3> :'a,'bs/\<<C-R><C-W>\>/<C-R><C-w>/gc
inoremap <F4> <C-R>=strftime("%Y-%b-%d %a %I:%M %p")<CR>
noremap <F12> :set spell! spelllang=en<CR>
runtime macros/matchit.vim

" Pathogen
"execute pathogen#infect()
syntax on
filetype off
filetype plugin indent off
filetype on
filetype plugin on
filetype plugin indent on
set backupdir=~/.tmp,./.backup,.,/tmp
set directory=~/.tmp,.,./.backup,/tmp

set clipboard=unnamedplus
set hlsearch
set incsearch
set lazyredraw
noremap - gT
noremap <c-space> gt
noremap <A-Left>  :tabmove 0<cr>
noremap <A-Right> :-tabmove<cr>
noremap <leader>t :Tex<CR>
noremap <C-s> :set buftype=" "<ESC>:w<CR><ESC>
inoremap <C-s> <Esc>:set buftype=" "<ESC> :w<CR><ESC>
inoremap jk <ESC>
inoremap <Esc> <nop>
set cursorcolumn

if has('gui_running')
        set background=dark
else
        set t_Co=16
        let g:solarized_termcolors=256
        set background=dark
endif
syntax on
colorscheme solarized
set ignorecase
set cmdheight=1
set laststatus=2
set wildmenu
set wildignore=*.dll,*.pyc,*.o
set wildmode=list:full
set cryptmethod=blowfish

"Tab
set expandtab
"
"Views
"autocmd BufWinLeave * mkview
"autocmd BufWinEnter * silent loadview
autocmd BufWinLeave * if expand("%") != "" | mkview | endif
autocmd BufWinEnter * if expand("%") != "" | loadview | endif

"Python Settings
autocmd BufEnter,BufRead,BufReadPre,BufNewFile *.py call PySettings()
function! PySettings()
        set syntax=python
        set expandtab
        set tabstop=4
        set shiftwidth=4
        set list
        set listchars=tab:>-,trail:-
        set smartindent
        "echohl WarningMsg | echo "Python settings with 4 tabs" | echohl None
endfunction

function! Py2Settings()
        set expandtab
        set tabstop=2
        set shiftwidth=2
        set list
        set listchars=tab:>-,trail:-
        echohl WarningMsg | echo "Python settings with 2 tabs" | echohl None
endfunction

map ,py2 :call Py2Settings()<CR>
map ,py4 :call PySettings()<CR>

autocmd BufWritePre *.html :normal mzgg=G`z

autocmd BufEnter,BufRead,BufWritePost,BufReadPre,BufNewFile *.html call HTMLSettings()
function! HTMLSettings()
        set expandtab
        set tabstop=2
        set shiftwidth=2
        set list
        set listchars=tab:>-,trail:-
        set cursorcolumn
        set syntax=html
endfunction

"Go Settings
autocmd BufEnter,BufRead,BufWritePost,BufReadPre,BufNewFile *.go call GoSettings()
function! GoSettings()
        set noexpandtab
        set tabstop=4
        set shiftwidth=4
        set list
        set listchars=tab:>-,trail:-

        nmap <leader>r <Plug>(go-run)
        nmap <leader>b <Plug>(go-build)
        nmap <leader>gt <Plug>(go-test)
        nmap <leader>c <Plug>(go-coverage)
        nmap <Leader>ds <Plug>(go-def-split)
        nmap <Leader>dv <Plug>(go-def-vertical)
        nmap <Leader>dt <Plug>(go-def-tab)

        nmap <Leader>gd <Plug>(go-doc)
        nmap <Leader>gv <Plug>(go-doc-vertical)

        nmap <Leader>gb <Plug>(go-doc-browser)
        nmap <Leader>s <Plug>(go-implements)

        nmap <Leader>i <Plug>(go-info)

        nmap <Leader>e <Plug>(go-rename)

        let g:go_fmt_command = "goimports"
        setlocal nolist

endfunction

map ,ele :e scp://ashishthedev@elevation.adaptinfrastructure.com//home/ashishthedev/elevation/elevation.py
map ,api :e scp://ashishthedev@elevation.adaptinfrastructure.com//home/ashishthedev/pdfserver/pdfserver/api.py
map ,emailer :e scp://ashishthedev@104.198.90.66//home/ashish_the_dev/wk/emailer/app/main/views.py
map ,sh :e scp://ashishthedev@elevation.adaptinfrastructure.com//home/ashishthedev/pdfserver/bin/livePdf.sh
map ,e2 :e scp://ashish@staging.adaptwater.com.au//tmp/g.txt
"map ,e3 :e scp://ashish@adaptwater.com.au//tmp/g.txt<CR>
map ,e3 :e scp://ashish@adaptwater.com.au//tmp/g.txt<CR>
  
"set guifont=Monospace:h20
"""""""""""""" Cursor Jazz """""""""""""""""""'
"To change the cursor in insert mode
"autocmd InsertEnter,InsertLeave ?* silent set cul!
":hi CursorLine cterm=NONE ctermbg=black

"if has("autocmd")
"  au InsertEnter * silent execute "!gconftool-2 --type string --set /apps/gnome-terminal/profiles/Default/cursor_shape ibeam"
"  au InsertLeave * silent execute "!gconftool-2 --type string --set /apps/gnome-terminal/profiles/Default/cursor_shape block"
"  au VimLeave * silent execute "!gconftool-2 --type string --set /apps/gnome-terminal/profiles/Default/cursor_shape block"
"endif
"set guicursor+=n-v-c:blinkon0


"""""""""""""" Syntastic Settings """""""""""""""""""'
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_mode_map = { 'passive_filetypes': ['python', 'javascript'] }
"""""""""""""" Syntastic Settings End """""""""""""""""""'



"""""""""""""" Javascript-vim """""""""""""""""""""""""""
autocmd BufEnter,BufRead,BufReadPre,BufNewFile *.js call JSSettings()
function! JSSettings()
        set tabstop=2
        set shiftwidth=2 
        let g:javascript_conceal_function             = "Æ’"
        let g:javascript_conceal_null                 = "Ã¸"
        let g:javascript_conceal_this                 = "@"
        let g:javascript_conceal_return               = "â‡š"
        let g:javascript_conceal_undefined            = "Â¿"
        let g:javascript_conceal_NaN                  = "â„•"
        let g:javascript_conceal_prototype            = "Â¶"
        let g:javascript_conceal_static               = "â€¢"
        let g:javascript_conceal_super                = "Î©"
        let g:javascript_conceal_arrow_function       = "â‡’"
        let g:javascript_conceal_noarg_arrow_function = "ðŸž…"
        let g:javascript_conceal_underscore_arrow_function = "ðŸž…"

        map <leader>l :exec &conceallevel ? "set conceallevel=0" : "set conceallevel=1"<CR>
endfunction

"""""""""""""" Javascript-vim """""""""""""""""""""""""""


map ,spell :setlocal spell spelllang=en<CR>
map ,nospell :set nospell<CR>
map ,title :s/\<\(\w\)\(\w*\)\>/\u\1\L\2/g<CR><CR>
